#!/usr/bin/env python
import rospy
#import customised msgs
from mini_bot.msg import RangeSensor
from mini_bot.msg import MotorControl

rospy.init_node('Motor_controller_with_Sonar', anonymous=True)
motor_control_pub = rospy.Publisher('motor_control', MotorControl, queue_size=10)

#mapping function of the distance to the speed of the wheels
def range_translate(val, lmin, lmax, nmin, nmax):
    lspan = lmax - lmin
    rspan = nmax - nmin 
    scaled_val = float(val - lmin) / float(lspan)
    return int(nmin + (scaled_val * rspan))

#call back function for the subscriber rospy.Subscriber("sonar_distance", RangeSensor, sonar_distance_cb)
#this will calculate the relavant direction and the speed of the wheels considering the subscribed data
def sonar_distance_cb(range):
    direction = "break"
    lspeed = 85
    rspeed = 85
    rospy.loginfo("Left30Dist: %i, FrontDist: %i, Right30Dist: %i." ,range.Left30Dist, range.FrontDist, range.Right30Dist )

    #forward
    if(range.FrontDist >= 50 and range.FrontDist <= 1000 and range.Left30Dist >= 50 and range.Right30Dist >=50):
        direction = "forward"
        lspeed = range_translate(range.FrontDist, 50, 1000, 85, 105)
        rspeed = lspeed


    elif(range.FrontDist >= 30 and range.FrontDist <= 1000):
        #turn right while moving forward
        if(range.Left30Dist >=25 and range.Left30Dist <=50):
            direction = "forward"
            lspeed = range_translate(range.Left30Dist, 25, 50, 95, 90)#invert PWM with higher speed in Left wheel to turn Right
            rspeed = range_translate(range.Left30Dist, 25, 50, 85, 90)
        #turn right
        elif(range.Left30Dist >=0 and range.Left30Dist <=25):
            direction = "right"
            lspeed = range_translate(range.Left30Dist, 0, 25, 90, 85)#invert PWM with higher speed at lower distance
            rspeed = lspeed
        
        #turn left while moving forward
        elif(range.Right30Dist >=25 and range.Right30Dist <=50):
            direction = "forward"
            lspeed = range_translate(range.Right30Dist, 25, 50, 95, 90)#invert PWM with higher speed in Left wheel to turn Right
            rspeed = range_translate(range.Right30Dist, 25, 50, 85, 90)
        #turn left 
        elif(range.Right30Dist >=0 and range.Right30Dist <=25):
            direction = "left"
            rspeed = range_translate(range.Right30Dist, 0, 25, 90, 85)#invert PWM with higher speed at lower distance
            lspeed = rspeed


    #backward
    elif(range.FrontDist >= 0 and range.FrontDist <= 30):
        direction = "backward"
        lspeed = range_translate(range.FrontDist, 0, 40, 95, 90) #invert PWM inputs to make maximum speed when distance is < than 40cm
        rspeed = lspeed
    # any error occured
    else:
        direction = "break"
    
    # call the publisher
    motor_speed_dir_pub(direction,lspeed,rspeed)
    
    

#control the motros
def motor_speed_dir_pub(dir,lspeed,rspeed):
    #To rotate a wheel forward
    #False
    #True
    dirPWM = MotorControl()
    if dir == "forward":
        dirPWM.LWheelDir1 = False
        dirPWM.LWheelDir2 = True

        dirPWM.RWheelDir1 = False
        dirPWM.RWheelDir2 = True

    elif dir == "backward":
        dirPWM.LWheelDir1 = True
        dirPWM.LWheelDir2 = False

        dirPWM.RWheelDir1 = True
        dirPWM.RWheelDir2 = False

    elif dir == "right":
        dirPWM.LWheelDir1 = False
        dirPWM.LWheelDir2 = True

        dirPWM.RWheelDir1 = True
        dirPWM.RWheelDir2 = False

    elif dir == "left":
        dirPWM.LWheelDir1 = True
        dirPWM.LWheelDir2 = False

        dirPWM.RWheelDir1 = False
        dirPWM.RWheelDir2 = True

    elif dir == "break":
        dirPWM.LWheelDir1 = False
        dirPWM.LWheelDir2 = False

        dirPWM.RWheelDir1 = False
        dirPWM.RWheelDir2 = False

    else:
        rospy.loginfo("Error: Not valid value recieved in for Publisher as the \"dir\".")
    
    dirPWM.LSpeed = lspeed
    dirPWM.RSpeed = rspeed

    rospy.loginfo(dirPWM)
    #publish the data to the Arduino
    motor_control_pub.publish(dirPWM)


#initialise and configure Sonar sensor listener
def sonar_listener():
    rospy.Subscriber("sonar_distance", RangeSensor, sonar_distance_cb)
    rospy.spin()



if __name__ == '__main__':
    #call the sonar sensor
    sonar_listener()
