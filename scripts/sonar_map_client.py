#!/usr/bin/env python

import rospy

#import the service Request and Response cretaed for communication
from mini_bot.srv import map_sonar
from mini_bot.srv import map_sonarRequest
from mini_bot.srv import map_sonarResponse

#import matplot lib to plot the map data
from matplotlib import pyplot as plt
import numpy as N

def create_map_client():
    #wait for the service
    rospy.wait_for_service('create_map')
    try:
        map_data = rospy.ServiceProxy('create_map', map_sonar)
        dist_array = map_data(True)
        return dist_array.distMap

    except rospy.ServiceException, e:
        print "Service call failed: %s" %e

if __name__ == "__main__":
    print "Requesting Map Data"
    y = create_map_client()
    x = N.arange(0, 180, 3, dtype=float) * N.pi / 180.0
    print(y)
    plt.axes(polar = True)
    plt.polar(x, y)
    plt.show()



    